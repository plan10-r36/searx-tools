# Searx Tools

The searx tools help you to maintain a privacy‐aware search in the web.

Searx[0]  is  mainly  hosted at searx.me, which creates a burden for the
hobby developers of this project. The  goal  is  to  decentralize.  Your
first  goal  should be to setup your own searx instance[1]. But, if your
instance is working, publish it. If you are the only user of  the  searx
instance  it  is easy to track you down. The privacy gain of searx is to
have many users and hide yourself behind  the  flare  of  common  search
queries to all kind of searxh websites.

The searx tools help you to maintain this.

## Usage

Initialize the ~/.searx-instances file:

	% searx-instances-update

Get a searx instance:

	% searx-instances-random

Plumb[2] the search for some query:

	% searx where is my house?

## Updating the .searc-instances file

You can use a cron job for this. Beware that as of writing (2017‐05‐13),
the instances website is really slow, so I decided to only download  the
working instances on demand.

## Replacing plumb[2]

Open the file »searx« and change »plumb« to for example »xdg-open«.

## Bugs

Send them to:

	Christoph Lohmann <20h@r-36.net>

Have fun!

[0] https://searx.me
[1] https://github.com/asciimoo/searx/wiki/Installation
[2] http://git.r-36.net/plumber

